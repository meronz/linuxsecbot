#!/usr/bin/env python3

import feedparser
import sys
import os
import re
import json
import logging
import shutil
from datetime import datetime
from time import mktime


# create logger
module_logger = logging.getLogger('feeds')


class FeedsReader():
    feeds = []

    def __init__(self, configPath=None, countersPath=None):
        if configPath is None:
            self.feedConfigPath = os.path.join(sys.path[0],
                                               'config', 'feeds.json')
        else:
            self.feedConfigPath = configPath

        if countersPath is None:
            self.feedCountersPath = os.path.join(sys.path[0],
                                                 'config', 'feedcounters.json')
        else:
            self.feedCountersPath = countersPath

        self._loadFeedCounters()

    def update(self):
        """Gets the updated info from the internet"""
        updates = []

        for feed in self.feeds:
            module_logger.info("Retrieving feeds for " + feed.name)
            entries = []

            fpRet = feedparser.parse(feed.url)

            if fpRet['status'] != 200:
                module_logger.warn("Error {0} on {1}"
                                   .format(fpRet['status'], feed.url))
                continue

            for fpEntry in fpRet['entries']:
                entry = Entry(fpEntry, feed)
                entries.append(entry)

            # Sort by entry Id since we can't be sure by date only
            entries.sort(key=lambda x: x.entryId, reverse=False)

            # New feed for which we don't have a record
            if feed.acronym not in self.feedCounters.keys():
                self.feedCounters[feed.acronym] = 0

            # Remove entries already sent
            entries = [x for x in entries
                       if int(x.entryId) > int(self.feedCounters[feed.acronym])
                       ]

            updates.append({
                'feed': feed,
                'entries': entries
                })

            module_logger.info("Got {0} updates for feed {1}!"
                               .format(len(entries), feed.name))

        return updates

    def loadConfigFile(self):
        self.feeds = []
        with open(self.feedConfigPath, 'r') as configFile:
            _feeds = json.load(configFile)['feeds']
            for f in _feeds:
                self.feeds.append(Feed(f))

    def updateConfigFile(self, _feeds):
        # Employ a tmp copy in case something goes wrong
        with open(self.feedConfigPath+".tmp", 'w') as configFile:

            if type(_feeds) is list:
                items = [dict(x) for x in _feeds]
                json.dump({'feeds': items}, configFile, indent=2)

            elif type(_feeds) is Feed:
                newfeeds = []
                for i in range(0, len(self.feeds)):
                    if self.feeds[i].name == _feeds.name:
                        newfeeds.append(_feeds)
                    else:
                        newfeeds.append(self.feeds[i])

                items = [dict(x) for x in newfeeds]
                json.dump({'feeds': items}, configFile, indent=2)

            else:
                raise ValueError("Only Feed or List[Feed] accepted!")

        # Finally overwrite file
        shutil.move(self.feedConfigPath+".tmp", self.feedConfigPath)

    def _loadFeedCounters(self):
        """
        Load feed counters from a file.
        Should be a dict where key is feed name and value is the counter.
        """

        self.feedCounters = {}
        if(os.access(self.feedCountersPath, os.F_OK)):
            with open(self.feedCountersPath, 'r') as countersFile:
                self.feedCounters = json.load(countersFile)
        else:
            module_logger.info("Counters file not found!")
            self.feedCounters = {}

    def updateCountersFile(self):
        """Write the counters to a json file."""
        # Employ a tmp copy in case something goes wrong
        with open(self.feedCountersPath+".tmp", 'w') as countersFile:
            json.dump(self.feedCounters, countersFile, indent=2)

        # Finally overwrite file
        shutil.move(self.feedCountersPath+".tmp", self.feedCountersPath)


class Feed():
    """Class representing one feed (one dict in the json list)"""

    def __init__(self, fromJson: {}):
        self.url = fromJson['url']
        self.acronym = fromJson['acronym']
        self.name = fromJson['name']
        self.tags = fromJson['tags']
        self.regexp = fromJson['regexp']

        self.__regexp__ = re.compile(self.regexp)

    def __str__(self):
        return self.name + ": " + self.url

    def __iter__(self):
        # first start by grabbing the Class items
        iters = dict((k, v) for k, v in self.__dict__.items() if k[:2] != '__')

        # now 'yield' through the items
        for k, v in iters.items():
            yield k, v


class Entry():
    """Class representing one entry (one item in the RSS feed)"""

    def __init__(self, entry: {}, feedConf: Feed):
        self.feedConf = feedConf

        reMatch = feedConf.__regexp__.match(entry['title'])

        if reMatch.group('acronym') != feedConf.acronym:
            raise ValueError("Wrong acronym {0} != {1}"
                             .format(reMatch.group('acronym'),
                                     feedConf.acronym))

        self.entryId = reMatch.group('id')
        self.num = reMatch.group('num') or ""
        self.name = reMatch.group('name') or ""
        self.title = entry['title']
        self.url = entry['link']

    def __str__(self):
        return self.name

    def tweet(self):
        remainingLenght = 279 - len(self.feedConf.tags)
        remainingLenght -= 23  # Link lenght is always 23
        remainingLenght -= 3   # Three newlines

        if len(self.title) + remainingLenght > 279:
            text = self.title[:279-remainingLenght] + '...'
        else:
            text = self.title

        return "{0}\r\n{1}\r\n\r\n{2}".format(
               text, self.feedConf.tags, self.url)
