#!/usr/bin/env python3

import json
import sys
import os
import time
import tweepy
import logging

from .feeds import FeedsReader


def BotRun(_debug=True):
    logging.basicConfig(stream=sys.stdout,
                        level=logging.DEBUG,
                        format='%(levelname)s %(asctime)s: %(message)s',
                        datefmt='%Y/%m/%d %H:%M:%S')

    # Load needed vars from Heroku Environment
    CONSUMER_KEY = os.environ['consumer_key']
    CONSUMER_SECRET = os.environ['consumer_secret']
    ACCESS_TOKEN = os.environ['access_token']
    ACCESS_TOKEN_SECRET = os.environ['access_token_secret']

    logging.info("Waking up!")

    DEBUG = True

    try:
        if os.environ['DEBUG'] is not None:
            logging.info("Running in debug mode")
    except:
        if _debug:
            logging.info("Running in debug mode")
        DEBUG = _debug

    # Contact Twitter, getting api object
    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
    api = tweepy.API(auth)

    fr = FeedsReader()
    fr.loadConfigFile()

    updates = fr.update()

    for feedUpdates in updates:
        sleepTime = 10

        for entry in feedUpdates['entries']:
            feed = feedUpdates['feed']
            text = entry.tweet()
            sent = False

            while not sent:
                try:
                    if not DEBUG:
                        api.update_status(text)
                    logging.info("Just tweeted: {0}".format(text))
                    fr.feedCounters[feed.acronym] = entry.entryId
                    fr.updateCountersFile()
                    logging.info("Updated feed {0} with lastItem {1}"
                                 .format(feed.acronym, entry.entryId))
                    sent = True

                except tweepy.error.RateLimitError as e:
                    sleepTime += 10
                    logging.warn("Rate limiting triggered!")
                    logging.info("Increasing sleep time to {0}"
                                 .format(sleepTime))

            time.sleep(sleepTime)

    logging.info("Back to sleep... zzZ")

if __name__ == '__main__':
    try:
        BotRun()
    except Exception as e:
        logging.error(e, exc_info=True)
