# LinuxSecBot

Twitter bot for Linux security advisories etc.
Currently it polls for two rss feeds, as described in the __feeds.json__ file.

To use it you must first create a configuration file:

## config.json
Contains twitter tokens and secrets in this format:

    {
    "log-filename" : "/var/log/twitterbot.log",
    "consumer_key" : " ",
    "consumer_secret" : " ",
    "access_token" : " ",
    "access_token_secret" : " "
    }
    
I run it as a cron job.