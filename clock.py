from apscheduler.schedulers.blocking import BlockingScheduler

sched = BlockingScheduler()


@sched.scheduled_job('interval', minutes=10)
def timed_job():
    from linuxsecbot import linuxsecbot
    linuxsecbot.BotRun(_debug=True)

sched.start()
