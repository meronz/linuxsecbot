import os
import sys
import json
import pytest
import tweepy


@pytest.fixture(scope="session", autouse=True)
def execute_before_any_test():
    os.chdir(os.path.join(sys.path[0], 'config'))


def test_get_twitter_feed():
    # Load needed vars from Heroku Environment
    CONSUMER_KEY = os.environ['consumer_key']
    CONSUMER_SECRET = os.environ['consumer_secret']
    ACCESS_TOKEN = os.environ['access_token']
    ACCESS_TOKEN_SECRET = os.environ['access_token_secret']

    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
    api = tweepy.API(auth)

    user = api.get_user('linuxsecbot')
    print(user.name)
