import os
import sys
import json
import pytest
from linuxsecbot.feeds import FeedsReader, Feed, Entry


@pytest.fixture(scope="session", autouse=True)
def execute_before_any_test():
    os.chdir(os.path.join(sys.path[0], 'tests', 'fixtures'))


def test_getfeeds():
    with open("feedCounters.json", 'w') as countersFile:
        json.dump({"DSA": 0000}, countersFile)

    fr = FeedsReader(configPath="feeds.json",
                     countersPath="feedCounters.json")
    fr.loadConfigFile()

    assert(len(fr.feedCounters) == 1)

    updates = fr.update()

    assert(len(fr.feedCounters) == 2)

    for up in updates:
        if up['feed'].acronym == "DSA":
            assert(len(up['entries']) > 0)

    with open("feedCounters.json", 'w') as countersFile:
        json.dump({"DSA": 9999}, countersFile)

    fr = FeedsReader(configPath="feeds.json",
                     countersPath="feedCounters.json")
    fr.loadConfigFile()

    updates = fr.update()

    for up in updates:
        if up['feed'].acronym == "DSA":
            assert(len(up['entries']) == 0)


def test_regexp():
    with open('feeds.json', 'r') as config_file:
        _feeds = json.load(config_file)['feeds']

        f = Feed(_feeds[0])

        reMatch = f.__regexp__.match("DSA-1234 testname test 123")
        assert(reMatch is not None)
        assert(reMatch.group('acronym') == "DSA")
        assert(reMatch.group('id') == "1234")
        assert(reMatch.group('num') is None)
        assert(reMatch.group('name') == "testname test 123")

        reMatch = f.__regexp__.match("DSA-1234")
        assert(reMatch is not None)
        assert(reMatch.group('acronym') == "DSA")
        assert(reMatch.group('id') == "1234")
        assert(reMatch.group('num') is None)
        assert(reMatch.group('name') is None)

        reMatch = f.__regexp__.match("DSA-1234-1")
        assert(reMatch is not None)
        assert(reMatch.group('acronym') == "DSA")
        assert(reMatch.group('id') == "1234")
        assert(reMatch.group('num') == "1")
        assert(reMatch.group('name') is None)

        reMatch = f.__regexp__.match("DSA-1234-1 testname")
        assert(reMatch is not None)
        assert(reMatch.group('acronym') == "DSA")
        assert(reMatch.group('id') == "1234")
        assert(reMatch.group('num') == "1")
        assert(reMatch.group('name') == "testname")

        f1 = Feed(_feeds[1])
        reMatch = f1.__regexp__.match("USN-1234-1: testname (test)")
        assert(reMatch is not None)
        assert(reMatch.group('acronym') == "USN")
        assert(reMatch.group('id') == "1234")
        assert(reMatch.group('num') == "1")
        assert(reMatch.group('name') == "testname (test)")


def test_entry_text_trimmed():
        entryId = "1234"
        num = "1"
        name = "testname is a perfectly valid security update because lorem"
        title = "ABC-1234-1: " + name
        url = "https://asasdasdasdasdasdasdasdasdasdasdasdasdasdasd"
        tags = "#Debian #Linux"

        if len(title) + 23 > 279:
            text = title[:251] + '...'
        else:
            text = title

        returnVal = " ".join(("{0}\r\n{1}\r\n\r\n{2}", title, tags, url))

        assert(len(returnVal) <= 280)


def test_datetime_parsing():
    with open('feeds.json', 'r') as config_file:
        _feeds = json.load(config_file)['feeds'][0]
        f = Feed(_feeds)


def test_json_update():
    fr = FeedsReader('feeds.json')
    fr.loadConfigFile()

    feeds = fr.feeds

    assert(len(feeds) == 2)
    assert(feeds[0].acronym == "DSA")
    feeds[0].acronym = "ABC"
    fr.updateConfigFile(feeds)
    assert(len(feeds) == 2)
    assert(feeds[0].acronym == "ABC")
    feeds[0].acronym = "DSA"
    fr.updateConfigFile(feeds)
    assert(len(feeds) == 2)
    assert(feeds[0].acronym == "DSA")


def test_counters_update():
    with open("feedCounters.json", 'w') as countersFile:
        json.dump({"DSA": '0000'}, countersFile)

    fr = FeedsReader(configPath="feeds.json",
                     countersPath="feedCounters.json")
    fr.loadConfigFile()

    fr.feedCounters['ABC'] = '1234'
    fr.feedCounters['DEF'] = '4321'
    fr.updateCountersFile()
    assert(fr.feedCounters['ABC'] == '1234')
    assert(fr.feedCounters['DEF'] == '4321')
